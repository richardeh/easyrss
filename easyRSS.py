"""
easyRSS.py
Read RSS feeds
Author: Richard Harrington
Date Created: 9/6/2013

"""

import urllib.request as ur

feeds={"CNN Top Stories":"http://rss.cnn.com/rss/cnn_topstories.rss",
       "CNN U.S.":"http://rss.cnn.com/rss/cnn_us.rss"}

def read_url_to_file(url,filename):
    mode="w"
    local_filename, headers=ur.urlretrieve("%s" % url)
    html = open(local_filename)
    with open(filename,'w') as f:
        for line in html:
            f.write(html.readline()+"\n")
    f.close()

for key,value in feeds.items():    
    read_url_to_file(value,key)
